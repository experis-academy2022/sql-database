# SQL database

SQL database is a group project to practice creating, accessing and modifying SQL database with C#. First part consists of SQL quaries for creating and modifying "SuperheroDB" database that has heroes, powers and assistants. Second part consists of access and modification of customer database. 

## Features
### SuperheroDB
- Create a database
- Create tables
- Create relationships between heroes, assistants and superpowers
- Insert entries
- Update entries
- Delete Entries

### Customer database 
- Get all customers data
- Get specific customers data by ID 
- Get specific sustomers data by name
- Get a certain amount of customers(page)
- Add a customer
- Update a customer
- Get a number of customers in each country
- Get customers who are the highest spenders
- Get the most popular genre of a given customer

## Running the app
First you need to install SQL Server and SQL Server Management Studio. Also you need to clone or download the project to your computer.
```
To use SuperheroDB quaries run them in numbered order using SQL Server Management Studio.
```
```
- To use Customer database run the "Chinook_SqlServer_AutoIncrementPKs" file with SQL Server Management Studio.
- Open the SQL-datareading folder in your IDE and use different tests available on Program.cs file. 
Uncomment the one you want and use needed arguments.
- Make sure the server name in SQL Server Management Studio connection is ending in ".\SQLEXPRESS". 
Optionally change the DataSource in ConnectionStringHelper.cs in Repositories folder.
```
## Contributors
- Maxim Anikin https://gitlab.com/MaximusAnakin
- Marie Puhakka https://gitlab.com/mariesusan
## License
[MIT](https://choosealicense.com/licenses/mit/)

USE SuperheroesDB

CREATE TABLE Superhero (
	SuperheroID int PRIMARY KEY IDENTITY(1,1),
	Name varchar(50) NOT NULL,
	Alias varchar(50),
	Origin varchar(50)
)
CREATE TABLE Assistant (
	AssistantID int PRIMARY KEY IDENTITY(1,1),
	Name varchar(50) NOT NULL
)
CREATE TABLE Power (
	PowerID int PRIMARY KEY IDENTITY(1,1),
	Name varchar(50) NOT NULL,
	Description varchar(100)
)
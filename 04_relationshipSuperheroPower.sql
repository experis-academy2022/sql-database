CREATE TABLE PowerRelationship
(
   PowerRelationshipID INT PRIMARY KEY IDENTITY(1,1),
   SuperheroID INT FOREIGN KEY REFERENCES Superhero(SuperheroID),
   PowerID INT FOREIGN KEY REFERENCES Power(PowerID)
)
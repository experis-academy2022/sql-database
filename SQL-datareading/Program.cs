﻿using SQL_datareading.Models;
using SQL_datareading.Repositories;

internal class Program
{
    private static void Main(string[] args) 
    {
        ICustomerRepository repository = new CustomerRepository();

        // 1:
        //TestSelectAll(repository);
        // 2:
        //TestSelect(repository);
        // 3: 
        // TestSelectByName(repository);
        // 4:
        //TestSelectSome(repository);
        // 5:
        //TestInsert(repository);
        // 6:
        // TestUpdate(repository);
        // 7:
        TestCountries(repository);
        // 8:
        // 9:
    }
    static void TestSelectSome(ICustomerRepository repository)
    {
        PrintCustomers(repository.GetSomeCustomers(10, 0));
    }
    static void TestSelectAll(ICustomerRepository repository)
    {
        PrintCustomers(repository.GetAllCustomers());
    }
    static void TestSelect(ICustomerRepository repository)
    {
        PrintCustomer(repository.GetCustomer("1"));
    }
    static void TestSelectByName(ICustomerRepository repository)
    {
        PrintCustomer(repository.GetCustomerByName("Jack"));
    }
    static void TestInsert(ICustomerRepository repository)
    {
        Customer test = new Customer()
        {
            FirstName = "Test",
            LastName = "Dude",
            Country = "Neverland",
            PostalCode = "666",
            Phone = "123-123",
            Email = "test@test.com"
        };
        if (repository.AddNewCustomer(test))
        {
            Console.WriteLine("Yay, insert worked");
            PrintCustomer(repository.GetCustomer("60"));
        }
        else
        {
            Console.WriteLine("Boo, insert didn't work!");
        }
    }


    static void TestUpdate(ICustomerRepository repository)
    {
        Customer test = repository.GetCustomer("1");
        
        if (repository.UpdateCustomer(test, "jorma"))
        {
            Console.WriteLine("update worked");
            PrintCustomer(repository.GetCustomer("1"));
        }
        else
        {
            Console.WriteLine("Boo!");
        }
    }


    static void TestCountries(ICustomerRepository repository)
    {
        repository.CustomersInEachCountry().OrderByDescending(Key => Key.Value).Select(i => $"{i.Key}: {i.Value}").ToList().ForEach(Console.WriteLine);
    }

    static void PrintCustomers(IEnumerable<Customer> customers)
    {
        foreach (Customer customer in customers)
        {
            PrintCustomer(customer);
        }
    }
    static void PrintCustomer(Customer customer)
    {
        Console.WriteLine($"--- {customer.CustomerId} {customer.FirstName} {customer.LastName} {customer.Country} {customer.Email} {customer.Phone} {customer.PostalCode} ---");
    }
}
﻿using SQL_datareading.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL_datareading.Repositories
{
    public interface ICustomerRepository
    {
        public Customer GetCustomer(string id);
        public Customer GetCustomerByName(string name);
        public List<Customer> GetAllCustomers();
        public List<Customer> GetSomeCustomers(int maxCount, int offset);
        public bool AddNewCustomer(Customer customer);
        public bool UpdateCustomer(Customer customer, string newName);

        public Dictionary<string, int> CustomersInEachCountry();

    }   
}

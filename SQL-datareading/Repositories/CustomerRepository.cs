﻿using Microsoft.Data.SqlClient;
using Microsoft.Identity.Client;
using SQL_datareading.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL_datareading.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        public List<Customer> GetAllCustomers()
        {
            List<Customer> custList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Handle result
                            while (reader.Read())
                            {
                               Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0).ToString();
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(3);
                                temp.PostalCode = reader.IsDBNull(4) ? "<missing>" : reader.GetString(4);
                                temp.Phone = reader.IsDBNull(5) ? "<missing>" : reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                custList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log error
                Console.WriteLine(ex.Message);
            }
            return custList;
        }
        public List<Customer> GetSomeCustomers(int maxCount, int offset)
        {
            List<Customer> custList = new List<Customer>();
            string sql = $"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer ORDER BY CustomerId OFFSET {offset} ROWS FETCH NEXT {maxCount} ROWS ONLY";
            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Handle result
                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0).ToString();
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(3);
                                temp.PostalCode = reader.IsDBNull(4) ? "<missing>" : reader.GetString(4);
                                temp.Phone = reader.IsDBNull(5) ? "<missing>" : reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                custList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log error
                Console.WriteLine(ex.Message);
            }
            return custList;
        }
        public Customer GetCustomer(string id)
        {
            Customer customer = new Customer();
            string sql = $"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId = {id}"; 
            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Handle result
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0).ToString();
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.IsDBNull(4) ? "<missing>" : reader.GetString(4);
                                customer.Phone = reader.IsDBNull(5) ? "<missing>" : reader.GetString(5);
                                customer.Email = reader.GetString(6);
                            }
                        }
                    }
                }
            } catch(SqlException ex)
            {
                // Log error
                Console.WriteLine(ex.Message);

            }
            return customer;
        }

        public Customer GetCustomerByName(string name)
        {
            Customer customer = new Customer();
            string sql = $"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE LOWER(CONCAT(FirstName, ' ', LastName)) LIKE LOWER('%{name}%')";
            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Handle result
                                customer.CustomerId = reader.GetInt32(0).ToString();
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.IsDBNull(4) ? "<missing>" : reader.GetString(4);
                                customer.Phone = reader.IsDBNull(5) ? "<missing>" : reader.GetString(5);
                                customer.Email = reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log error
                Console.WriteLine(ex.Message);

            }
            return customer;
        }
        public bool AddNewCustomer(Customer customer)
        {
            bool success = false;
            string sql = $"INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email)" +
                "VALUES(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log error
                Console.WriteLine(ex.Message);
            }
            return success;
        }
        public bool UpdateCustomer(Customer customer, string newName)
        {
            bool success = false;
            string sql = $"UPDATE Customer SET FirstName = @newName WHERE CustomerId = @customerID";
            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@newName", newName);
                        cmd.Parameters.AddWithValue("@customerID", customer.CustomerId);

                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log error
                Console.WriteLine(ex.Message);
            }
            return success;
        }



        public Dictionary<string, int> CustomersInEachCountry()
        {
            Dictionary<string, int> countries = new Dictionary<string, int>();
            string sql = "SELECT Country, COUNT(*) FROM Customer GROUP BY Country";
            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Handle result
                            while (reader.Read())
                            {
                                string country = reader.GetString(0);
                                int count = reader.GetInt32(1);
                                countries.Add(country, count);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log error
                Console.WriteLine(ex.Message);
            }
            return countries;
        }


    }
}
